<%@ page import="com.alibaba.fastjson.JSONObject" %>
<%@ page import="com.raycloud.xss.websocket.MyServer" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    MyServer socketServer = (MyServer) application.getAttribute("ws");
    String nick = new String(request.getParameter("nick").getBytes("ISO-8859-1"));
    String password =new String(request.getParameter("password").getBytes("ISO-8859-1"));
    if (!"".equals(nick) && !"".equals(password)) {
        JSONObject object = new JSONObject();
        System.out.println(nick);
        object.put("nick", nick);
        object.put("password", password);
        socketServer.sendToAll(object.toJSONString());
        application.setAttribute("text", object.toJSONString());
        out.print("ok");
    } else {
        out.print("error");
    }
%>
