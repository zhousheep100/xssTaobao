/**
 * 通过chromeAPI获取cookie
 * 由于content.js得不到部分chrome对象所以从content发送一个事件来通信
 * 数据获取之后通过回调传递给前台，或者在本JS直接跨域调用
 */
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
    chrome.tabs.getSelected(function (d) {
        url = d.url;
        var obj={};
        obj.url=url;
        chrome.cookies.getAll(obj,function (k) {
            a="";
            for(var i =0;i< k.length;i++){
                a +=  k[i].name+"="+ k[i].value+";"
            }
            sendResponse({result:a});
        })
    });
    return true;
});
